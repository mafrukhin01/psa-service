global using PSAService.Models;
global using PSAService.Data;
global using PSAService.Dtos.MasterAttribute;
global using Microsoft.EntityFrameworkCore;
global using AutoMapper;
global using System.ComponentModel.DataAnnotations.Schema;
global using PSAService.Dtos;
using PSAService.Services.MasterDataService;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<DataContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddDbContext<SDEContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("SDEConnection")));
builder.Services.AddDbContext<DevHMSContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DevHMSConnection")));
builder.Services.AddControllers();

// Add services to the container.

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddScoped<IService, Service>();
builder.Services.AddHttpContextAccessor();
var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
app.UseSwagger();
app.UseSwaggerUI();
app.UseStaticFiles();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
