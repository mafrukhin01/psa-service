﻿using System;
namespace PSAService.Data
{
	public class DevHMSContext : DbContext
    {
        public DevHMSContext(DbContextOptions<DevHMSContext> options) : base(options) { }
        public DbSet<SyncBlockAncak> Blocks => Set<SyncBlockAncak>();
        public DbSet<MasterBlockAncak> MasterBlocks => Set<MasterBlockAncak>();
        public DbSet<SyncBlockAncakHistory> BlocksHistory => Set<SyncBlockAncakHistory>();
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterBlockAncak>().HasNoKey();
            modelBuilder.Entity<SyncBlockAncak>().HasNoKey();
            modelBuilder.Entity<SyncBlockAncakHistory>().HasNoKey();

        }



    }
}

