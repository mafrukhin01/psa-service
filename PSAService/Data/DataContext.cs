﻿using Microsoft.EntityFrameworkCore;

namespace PSAService.Data
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<AppMapping> AppMappings { get; set; }
        public DbSet<MasterAttribute> MasterAttributes => Set<MasterAttribute>();
        public DbSet<LogVersioning> LogVersionings => Set<LogVersioning>();
        public DbSet<UserAccess> UserAccesses => Set<UserAccess>();
        public DbSet<PostImage> PostImages => Set<PostImage>();
        public DbSet<MappingEstate> EstateMapping => Set<MappingEstate>();
        public DbSet<AppUpdate> AppUpdates => Set<AppUpdate>();
        public DbSet<UserProject> UserProjects => Set<UserProject>();
        public DbSet<UserAccessAncak> UserAccessAncak => Set<UserAccessAncak>();



        //public DbSet<MasterAttribute> MasterAttributes => Set<MasterAttribute>();
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterAttribute>().HasKey(x => x.AttributeId);
            modelBuilder.Entity<LogVersioning>().Property(c => c.CreatedDate).HasColumnType("datetime2(7)");
            modelBuilder.Entity<LogVersioning>().HasKey(x => x.IdRecord);
            //modelBuilder.Entity<UserAccess>().Property(c => c.startDate).HasColumnType("date");
            //modelBuilder.Entity<UserAccess>().Property(c => c.endDate).HasColumnType("date");
            //modelBuilder.ApplyConfiguration(new UserAccessDto.UserAccessDtoConfiguration());
            //modelBuilder.Entity<UserAccess>().Ignore(e => e.startAsLong);
            //modelBuilder.Entity<UserAccess>().Ignore(e => e.endAsLong);
            modelBuilder.Entity<UserAccess>().HasNoKey();
            modelBuilder.Entity<UserAccessAncak>().HasNoKey();
            modelBuilder.Entity<PostImage>().HasKey(c=>c.ImageId);
            modelBuilder.Entity<AppMapping>().HasNoKey();
            modelBuilder.Entity<AppUpdate>().HasNoKey();
            modelBuilder.Entity<UserProject>().HasNoKey();
            modelBuilder.Entity<ExtendEstate>().HasNoKey();


        }
    }
}
