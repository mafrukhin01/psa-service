﻿namespace PSAService.Data
{
    public class SDEContext : DbContext
    {
        public SDEContext(DbContextOptions<SDEContext> options) : base(options){}
        public DbSet<ObjectCountSDE> ObjectCountSDEs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ObjectCountSDE>().HasNoKey();
            modelBuilder.Entity<MasterBlockSDE>().HasNoKey();

        }
        
    }
}
