﻿namespace PSAService.Models
{
    [Table("T_PSA_Images")]
    public class Images
    {
	public string? UniqueID { get; set; }
	public int CreateBy { get; set; }
	public DateTime? CreateDate { get; set; }
	public string? EstCode { get; set; }
	public string? ImgLocation { get; set; }
	public string? ImgName { get; set; }
	public string? GlobalId { get; set; }
	public int ImgSize { get; set; }
	}
}
