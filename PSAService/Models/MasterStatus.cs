﻿namespace PSAService.Models
{
    public class MasterStatus
    {
        public int StatusId { get; set; }
        public string? AliasCode { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
