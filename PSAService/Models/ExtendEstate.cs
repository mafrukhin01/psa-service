﻿namespace PSAService.Models
{
    public class ExtendEstate
    {
        public string MinX { get; set; }
        public string MinY { get; set; }
        public string MaxX { get; set; }
        public string MaxY { get; set; }
    
    }
}
