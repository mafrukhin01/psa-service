﻿namespace PSAService.Models
{
    public class PostBlockSync
    {
        public string userId { get; set; }
        public string block { get; set; }
        public string estate { get; set; }
        public int action { get; set;}
    }
}
