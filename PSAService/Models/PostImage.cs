﻿namespace PSAService.Models
{
    [Table("T_PSA_Images")]
    public class PostImage
    {
        public Int64 ImageId { get; set; }
        public string SessionSurveyId { get; set; }
	    public string GlobalID { get; set; }
	    public string Location { get; set; }
        public int isDeleted { get; set; }
    }
}
