﻿using System.ComponentModel.DataAnnotations;

namespace PSAService.Models
{
    public class UserProject
    {
        [Table("T_PSA_UserAccess")]
        public class UserAccess
        {
            [Key, Column(Order = 0)]
            public string estCode { get; set; }

            [Key, Column(Order = 1)]
            public string companyCode { get; set; }

            [Key, Column(Order = 2)]
            public long userID { get; set; }
            public string codeModule { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public int? statusEdit { get; set; }
            public DateTime? startDate { get; set; }
            public DateTime? endDate { get; set; }
        }
    }
}
