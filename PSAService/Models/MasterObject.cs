﻿namespace PSAService.Models
{
    public class MasterObject
    {

      public int MasterObjectId { get; set; }
      public string? MDMCode { get; set; }
      public string? Name { get; set; }
      public string? Description { get; set; }
      public string? GeometryType { get; set; }
      public string? AttributeCollection { get; set; }
      public string? ConditionCollection { get; set; }
      public string? MaterialCollection { get; set; }
      public string? TypeCollection { get; set; }
      public string? StatusCollection { get; set; }
    }
}
