﻿namespace PSAService.Models
{
    public class ObjectCountSDE
    {
        public int TotalBRD1 { get; set; }
        public int TotalCVT1 { get; set; }
        public int TotalPUM1 { get; set; }
        public int TotalWGT1 { get; set; }
        public int TotalDAM1 { get; set; }
        public int TotalBUN1 { get; set; }
        public int TotalZIP1 { get; set; }


    }
}
