﻿
namespace PSAService.Models
{

    [Table("T_PSA_Master_Attribute")]
    public class MasterAttribute
    {
        
        public int AttributeId { get; set; }
        public string? AliasCode { get; set; }
        public byte IsMandatory { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? UnitOfMeasurement { get; set; }
        public string? SymbolUOM { get; set; }

    }


}
