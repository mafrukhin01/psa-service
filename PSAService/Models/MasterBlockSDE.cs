﻿namespace PSAService.Models
{
    public class MasterBlockSDE
    {
        public string Block { get; set; }
        public string ESTNR { get; set; }
        public string Division { get; set;}
        public string CompanyCode { get; set;}
        public decimal? Ha_Gross { get; set;}
        public decimal? Ha_GIS { get; set;}

    }
}
