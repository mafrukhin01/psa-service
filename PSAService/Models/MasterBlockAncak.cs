﻿namespace PSAService.Models
{
    [Table("T_HMS_TPH_MasterBlockAncak")]
    public class MasterBlockAncak
    {
        public string? estCode { get; set; }
        public string? block { get; set; }
        public int? multiAncak { get; set; }
        public string? mandor { get; set; }
        public int? createBy { get; set; }
        public DateTime? createDate { get; set; }
        public DateTime? startPeriode { get; set; }
        public DateTime? endPeriode { get; set; }



    }
}
