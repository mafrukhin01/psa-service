﻿namespace PSAService.Models
{
    public class MasterType
    {
        public int TypeId { get; set; }
	    public string? AliasCode { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
