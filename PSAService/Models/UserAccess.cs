﻿namespace PSAService.Models
{

    [Table("T_AWM_R_Edit")]
    public class UserAccess
    {
        public string estCode { get; set; }
        public string companyCode { get; set; }
        public Int64 userID { get; set; }
        public int statusEdit { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        //[NotMapped]
        //public long startAsLong
        //{
        //    get { return startDate.Ticks; }
        //}
        //[NotMapped]
        //public long endAsLong
        //{
        //    get { return endDate.Ticks; }
        //}


    }
}
