﻿namespace PSAService.Models
{
    public class ImageUpload
    {
        //public int ImageId { get; set; }
        public string Name { get; set; }
        public string SessionSurveyId { get; set; }
        public string Image { get; set; }
        public string GlobalID { get; set; }


    }
}
