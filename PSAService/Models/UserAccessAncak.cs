﻿using System;
namespace PSAService.Models
{
    [Table("T_PSA_Project_Ancak")]
    public class UserAccessAncak
    {
        public string estCode { get; set; }
        public string companyCode { get; set; }
        public Int64 userID { get; set; }
        public int statusEdit { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        //[NotMapped]
        //public long startAsLong
        //{
        //    get { return startDate.Ticks; }
        //}
        //[NotMapped]
        //public long endAsLong
        //{
        //    get { return endDate.Ticks; }
        //}


    }
}

