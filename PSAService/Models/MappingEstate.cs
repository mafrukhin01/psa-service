﻿namespace PSAService.Models
{
    [Table("T_COR_EstateMapping_New")]
    public class MappingEstate
    {
        public int ID { get; set; }
        public string? Plantation { get; set; }
        public string? Grouping { get; set; }
        public string? GroupCompanyName { get; set; }
        public string? CompanyCode { get; set; }
        public string? EstCode { get; set; }
        public string? EstNewCode { get; set; }
        public string? EstCodeSAP { get; set; }
        public string? NewEstName { get; set; }
        public string? SDERegion { get; set; }
        public int? RegionID { get; set; }

    }
}
