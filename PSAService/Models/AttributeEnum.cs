﻿using System.Text.Json.Serialization;

namespace PSAService.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AttributeEnum
    {
        Show = 0,
        Hide = 1
    }
}
