﻿using System;
namespace PSAService.Models
{
    [Table("T_HMS_TPH_BlockCheckout_History")]
    public class SyncBlockAncakHistory
    {
        public int idHistory { get; set; }
        public string estCode { get; set; }
        public string block { get; set; }
        public int userActive { get; set; }
        public DateTime lastCheckout { get; set; }
        public DateTime lastCheckin { get; set; }
        public DateTime userApprove { get; set; }
        public DateTime dateApprove { get; set; }
        public int status { get; set; }
        public string remarks { get; set; }



    }
}

