﻿namespace PSAService.Models
{
    [Table("T_PSA_APP_Version")]
    public class AppUpdate
    {
        public string? VersionCode { get; set; }
        public string? VersionName { get; set; }
        public string? Description { get; set; }
        public byte? IsMandatory { get; set; }
    }
}
