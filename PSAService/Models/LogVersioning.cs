﻿namespace PSAService.Models
{
    [Table("T_PSA_Record_Versioning")]
    public class LogVersioning
    {
        public string? IdRecord { get; set; }
        public string? UserId { get; set; }
        public byte Status { get; set; }
        public string? Versioning { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
}
