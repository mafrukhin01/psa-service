﻿namespace PSAService.Models
{
    public class MasterMaterial
    {
      public int MaterialId { get; set; }    
	  public string? AliasCode { get; set; }
	  public string? Name { get; set; }
	  public string? Description { get; set; }
    }
}
