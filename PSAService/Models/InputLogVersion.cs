﻿namespace PSAService.Models
{
    public class InputLogVersion
    {
        public string? IdRecord { get; set; }
        public string? UserId { get; set; }
        public byte Status { get; set; }
        public string? Versioning { get; set; }
        public long CreatedDate { get; set; }
    }
}
