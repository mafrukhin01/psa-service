﻿namespace PSAService.Models
{
    [Table("T_PSA_Master_Condition")]
   
    public class MasterCondition
    {
        public int TypeId
        {
            get; set;
        }
        public string? AliasCode
        {
            get; set;
        }
        public string? Name
        {
            get; set;
        }
        public string? Description
        {
            get; set;
        }
    }
}
