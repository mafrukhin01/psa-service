﻿namespace PSAService.Models
{
    public class ServiceResponse<T>
    {
        
        public bool Success { get; set; } = true;
        public string Message { get; set; } = string.Empty;
        public T? Data { get; set; }
        //public string Error { get; set; } = string.Empty;




    }
}
