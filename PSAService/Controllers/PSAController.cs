﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Web;
using PSAService.Models;
using PSAService.Services.MasterDataService;
using System.Text.Json.Nodes;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Data;

namespace PSAService.Controllers
{
    [Route("api/v1/")]
    [ApiController]
    public class PSAController : ControllerBase
    {

        private readonly IService _masterDataService;
        private readonly DataContext _context;
        private readonly SDEContext _sdeContext;
        private readonly DevHMSContext _devHMSContext;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public PSAController(IService masterDataService, DataContext context, SDEContext sdeContext, DevHMSContext devHMSContext,  IWebHostEnvironment webHostEnvironment)
        {
            _masterDataService = masterDataService;
            _context = context;
            _hostingEnvironment = webHostEnvironment;
            _sdeContext = sdeContext;
            _devHMSContext = devHMSContext;
        }


        [HttpGet("getAll")]
        public async Task<ActionResult<ServiceResponse<List<GetMasterAttributeDto>>>> GetAll()
        {
            return Ok(await _masterDataService.GetAll());
        }

        [HttpGet("getAll2")]
        public async Task<ActionResult<ServiceResponse<dynamic>>> GetAll2()
        {
            return Ok(await _masterDataService.GetAll2());
        }

        [HttpGet("getVersion")]
        public async Task<ActionResult<string>> GetVersion()
        {
            //string imgpath = System.Web.Hosting.HostingEnvironment.MapPath("~/Uploads/Images/" + "" + "/");
            //System.IO.Directory.CreateDirectory();
            return Ok( "");
        }

        [HttpGet("getMasterBlockSync")]
        public async Task<ActionResult<string>> GetMasterBlockSync()
        {
            //string imgpath = System.Web.Hosting.HostingEnvironment.MapPath("~/Uploads/Images/" + "" + "/");
            //System.IO.Directory.CreateDirectory();
            return Ok("GET MASTER BLOCK");
        }


        [HttpGet("getMasterMaterial")]
        public async Task<ActionResult> GetMaster()
        {
            //var serviceResponse = new ServiceResponse<JObject>();
            //var dbCharacters = await _context.MasterAttributes.ToListAsync();
            //var list = dbCharacters.Select(c => _mapper.Map<GetMasterAttributeDto>(c)).ToList();
            //string jsonFieldValue = JsonConvert.SerializeObject(dbCharacters);
            //Console.WriteLine("jsonField", (jsonFieldValue));
            //Console.WriteLine("db", dbCharacters.Count);
            ////var myJsonObject = new
            ////{
            ////    masterAttribute = jsonFieldValue
            ////};

            //string json = JsonConvert.SerializeObject(myJsonObject);
            JObject obj = new JObject();
            obj["masterAttribute"] = "a";
            obj["masterAttribute2"] = "q";

            
            return Ok(obj);
        }

        [HttpGet("getObject")]
        public ActionResult<dynamic> GetObject()
        {
            // Create a new JObject

            // Create a new JObject
            var jsonObject = new Dictionary<string, object>();

            // Add attributes and values to the dictionary
            jsonObject["attribute1"] = "value1";
            jsonObject["attribute2"] = "value2";
            jsonObject["attribute3"] = 123;

            return jsonObject; // Retur
            // Return the dynamic object as the JSON response
           
           
        }
        [HttpGet("getMasterType")]
        public async Task<ActionResult<ServiceResponse<List<GetMasterAttributeDto>>>> Gets()
        {
            return Ok(_masterDataService.GetMasterAttributes);
        }
        [HttpGet("getMasterObject")]
        public async Task<ActionResult<ServiceResponse<List<GetMasterAttributeDto>>>> Get()
        {
            return Ok(_masterDataService.GetMasterAttributes);
        }
        [HttpGet("getMasterEvent")]
        public async Task<ActionResult<ServiceResponse<List<GetMasterAttributeDto>>>> GetMasters()
        {
            return Ok(_masterDataService.GetMasterAttributes);
        }
        [HttpGet("getMasterStatus")]
        public async Task<ActionResult<ServiceResponse<List<GetMasterAttributeDto>>>> GetMs()
        {

            return Ok(_masterDataService.GetMasterAttributes);
        }
        [HttpGet("getTest")]
        public async Task<ActionResult<IEnumerable<MasterAttribute>>> GetMasterAttribute()
        {
            return await _context.MasterAttributes.ToListAsync();
        }

        [HttpGet("getUserAccess")]
        public async Task<ActionResult<ServiceResponse<List<UserAccess>>>> GetUserAccess()
        {
            return Ok(await _masterDataService.GetUserAccess());
        }
        [HttpGet("getUserAccessDto")]
        public async Task<ActionResult<ServiceResponse<List<UserAccessDto>>>> GetUserAccessDto()
        {
            return Ok(await _masterDataService.GetUserAccessDto());
        }

        [HttpGet("getProjectsByUserEstate")]
        public async Task<ActionResult<ServiceResponse<dynamic>>> GetProjectsByUserOld(Int64 userId, string estCode)
        {
            ServiceResponse<dynamic> result = await _masterDataService.GetProjectMappingByUser(userId, estCode);
            if (result.Success == true)
            {
                return Ok(await _masterDataService.GetProjectMappingByUser(userId, estCode));
            }
            else
            {
                return NotFound("Projects invalid!");
            }
                
        }
        [HttpGet("getProjectsByUserEstate2")]
        public async Task<ActionResult<ServiceResponse<dynamic>>> GetProjectsByUser3(Int64 userId, string estCode)
        {
            ServiceResponse<dynamic> result = await _masterDataService.GetProjectMappingByUser2(userId, estCode);
            if (result.Success == true)
            {
                return Ok(await _masterDataService.GetProjectMappingByUser2(userId, estCode));
            }
            else
            {
                return NotFound("Projects invalid!");
            }

        }

        [HttpGet("getProjectsByUser")]
        public async Task<ActionResult<ServiceResponse<List<UserAccessDto>>>> GetProjectsByUser(Int64 userId, string estCode)
        {
            return Ok(await _masterDataService.GetProjectsByUser(userId, estCode));
        }

        [HttpGet("getProjectsByUser2")]
        public async Task<ActionResult<ServiceResponse<List<UserAccessDto>>>> GetProjectsByUser2(Int64 userId, string estCode)
        {
            return Ok(await _masterDataService.GetProjectsByUser2(userId, estCode));
        }




        [HttpPost("postRecordSDE")]
        public async Task<ActionResult<LogVersioning>> PostVersion(InputLogVersion request)
        {
            //var response = await _authRepo.Login(request.Username, request.Password);
            /*long unixTimeMillis = 1652130600000;*/ // May 9, 2022 2:30:00 PM UTC in Unix timestamp format
            DateTime dateTime = ConvertUnixTimeToDateTime(request.CreatedDate);
            LogVersioning logVersioning = new LogVersioning()
            {
                IdRecord = request.IdRecord,
                Versioning = request.Versioning,
                UserId = request.UserId,
                Status = request.Status,
                CreatedDate = dateTime


            };
            _context.LogVersionings.Add(logVersioning);
            await _context.SaveChangesAsync();

            return Ok(await _context.SaveChangesAsync());

        }

        [HttpGet("getUserProject")]
        public async Task<ActionResult<ServiceResponse<List<UserAccessDto>>>> GetUserProject(Int64 userId, string estCode)
        {
            return Ok(await _masterDataService.GetProjectsByUser(userId, estCode));
        }



        [HttpGet("GetMasterBlocksAncak")]
        public async Task<ActionResult<ServiceResponse<List<MasterBlocksDto>>>> GetMasterBlocksAncak(string estnr)
        {
            return Ok(await _masterDataService.GetMasterBlocksAncak(estnr));
            // Call the service method
            // var response = await _masterDataService.GetMasterBlocksAncak(estnr);

            // Check if the service response was successful
            /* if (response.Success)
             {
                 return Ok(response.Data);
             }
             else
             {
                 return NotFound(response.Message);
             }*/
        }

        //[HttpPost("postMaster")]
        //public async Task<ActionResult<ServiceResponse>>


        public static DateTime ConvertUnixTimeToDateTime(long unixTimeMillis)
        {
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(unixTimeMillis);
            return dateTimeOffset.ToOffset(TimeSpan.FromHours(7)).DateTime;
        }

        private readonly string imageBasePath = "https://example.com/images"; // Base URL for the images

        [HttpGet("getImage/{imageName}")]
        public IActionResult GetImage(string imageName)
        {

            // Get the full path of the image file
            string imagePath = Path.Combine(imageBasePath, imageName);

            // Check if the image file exists
            if (System.IO.File.Exists(imagePath))
            {
                // Read the image file content
                byte[] imageBytes = System.IO.File.ReadAllBytes(imagePath);

                // Determine the content type based on the file extension (e.g., "image/jpeg", "image/png", etc.)
                string contentType = GetImageContentType(imageName);
                Guid.NewGuid();
                // Return the image file as a response with the appropriate content type
                return File(imageBytes, contentType);
            }
            else
            {
                // Image file not found, return a response indicating the error
                return NotFound();
            }
        }

        
        private string GetImageContentType(string imageName)
        {
            // Get the file extension from the image file name
            string extension = Path.GetExtension(imageName);

            // Map common image file extensions to their corresponding content types
            switch (extension.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                case ".gif":
                    return "image/gif";
                // Add more cases for other image file extensions if needed
                default:
                    return "application/octet-stream"; // Default content type for unknown file types
            }
        }

        [HttpGet("getAppVersion")]
        public IActionResult GetAppVersion(string package)
        {
            var sqlQuery = "SELECT VersionCode, Version FROM [gis_app].[dbo].[T_MOB_AMS_APPS] WHERE PackageName = @param";
            var result = _context.Set<AppMapping>().FromSqlRaw(sqlQuery, new SqlParameter("@param", package)).ToList();
            if (result.Count > 0)
            {
                return (Ok(result));
            }
            else
            {
                return (NotFound("PackageName invalid!"));
            }
           
        }
        [HttpGet("CheckUpdateApp")]
        public IActionResult GetAppUpdate()
        {
            var sqlQuery = "SELECT VersionCode, Version FROM [gis_app].[dbo].[T_MOB_AMS_APPS] WHERE PackageName = 'id.co.ams_plantation.plantation_survey_app'";
            var result = _context.Set<AppMapping>().FromSqlRaw(sqlQuery).ToList();
            if (result.Count > 0)
            {
                AppMapping _app = result.FirstOrDefault();
                var db = _context.AppUpdates.Where(p => p.VersionCode == _app.Version).ToList();
                return (Ok(db));
            }
            else
            {
                return (NotFound("PackageName invalid!"));
            }

        }
        [HttpGet("getCountObjectSDE")]
        public IActionResult GetCountObjectSDE()
        {
            var sqlQuery = "EXEC KPN_Plantation.sde.set_current_version 'Esri_Anonymous_pub_psa/PA_Inve'" +
                "Select(SELECT COUNT(*) FROM KPN_Plantation.sde.BRD1_evw)AS TotalBRD1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.CVT1_evw)AS TotalCVT1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.PUM1_evw)AS TotalPUM1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.WGT1_evw)AS TotalWGT1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.DAM1_evw)AS TotalDAM1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.BUN1_evw)AS TotalBUN1," +
                "(SELECT COUNT(*) FROM KPN_Plantation.sde.ZIP1_evw)AS TotalZIP1; ";

            //var execQuery = "EXEC KPN_Plantation.sde.set_current_version 'Esri_Anonymous_pub_psa/PA_Inve'";
            //var selectQuery = "SELECT " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.BRD1_evw) AS TotalBRD1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.CVT1_evw) AS TotalCVT1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.PUM1_evw) AS TotalPUM1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.WGT1_evw) AS TotalWGT1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.DAM1_evw) AS TotalDAM1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.BUN1_evw) AS TotalBUN1, " +
            //                  "(SELECT COUNT(*) FROM KPN_Plantation.sde.ZIP1_evw) AS TotalZIP1";
            //var query = _sdeContext.Database.SqlQuery<ObjectCountSDE>(sqlQuery).ToList(); ;
            //var result = query.ToList();

            var result = _sdeContext.Set<ObjectCountSDE>().FromSqlRaw(sqlQuery).ToList();
            if (result.Count > 0)
            {
                return (Ok(result));
            }
            else
            {
                return (NotFound("PackageName invalid!"));
            }

        }


        [HttpGet("GetMasterBlockSDE")]
        public IActionResult GetMasterBlockSDE(string estnr)
        {
            // Eksekusi perintah EXEC terpisah
            var execQuery = "EXEC KPN_Plantation.sde.set_current_version 'Esri_Anonymous_pub_psa/Block_A'";
            _sdeContext.Database.ExecuteSqlRaw(execQuery);

            // Query untuk SELECT
            var sqlQuery = "SELECT Block, ESTNR, Division, CompanyCode, Ha_Gross, Ha_GIS " +
                           "FROM KPN_Plantation.sde.BLK3_evw " +
                           "WHERE ESTNR = @param";

            // Eksekusi query SELECT dengan parameter
            var result = _sdeContext.Set<MasterBlockSDE>().FromSqlRaw(sqlQuery, new SqlParameter("@param", estnr)).ToList();

            if (result.Count > 0)
            {

                var nonNullResult = result.Select(r => new
                {
                    Block = r.Block ?? "N/A",  // Atau penanganan lain yang sesuai
                    ESTNR = r.ESTNR ?? "N/A",
                    Division = r.Division ?? "N/A",
                    CompanyCode = r.CompanyCode ?? "N/A",
                    Ha_Gross = r.Ha_Gross ?? 0.0M,  // Penanganan untuk nullable decimal
                    Ha_GIS = r.Ha_GIS ?? 0.0M         // Asumsi tipe data numerik
                }).ToList();

                return (Ok(result));
            }
            else
            {
                return (NotFound("invalid request!"));
            }

        }

        [HttpGet("GetExtend")]
        public IActionResult GetExtend(string estnr)
        {
          
            // Query untuk SELECT
            var sqlQuery = "select MinX, MinY, MaxX, MaxY from T_COR_Estate " +
                           "WHERE EstCode = @param";

            // Eksekusi query SELECT dengan parameter
            var result = _context.Set<ExtendEstate>().FromSqlRaw(sqlQuery, new SqlParameter("@param", estnr)).ToList();

            if (result.Count > 0)
            {

                var nonNullResult = result.Select(r => new
                {
                    MinX = r.MinX ?? "N/A",  // Atau penanganan lain yang sesuai
                    MinY = r.MinY ?? "N/A",
                    MaxX = r.MaxX ?? "N/A",
                    MaxY = r.MaxY ?? "N/A",
                   // Asumsi tipe data numerik
                }).ToList();

                return (Ok(result));
            }
            else
            {
                return (NotFound("invalid request!"));
            }

        }

        [HttpGet("getAllEstateMapping")]
        public async Task<ActionResult<ServiceResponse<List<EstateMappingDto>>>> GetAllEstateMapping()
        {
            return Ok(await _masterDataService.GetAllEstateMapping());


        }


        [HttpGet("getEstateMappingByEstCode")]
        public async Task<ActionResult<ServiceResponse<List<EstateMappingDto>>>> GetEstateMappingByEstCode(string estCode)
        {
            return Ok(await _masterDataService.GetEstateMappingByEstCode(estCode));


        }



        [HttpPost("postImages")]
        public async Task<IActionResult> PostImage([FromBody] List<ImageUpload> models)
        {
            try
            {
                foreach (ImageUpload upload in models)
                {
                    if (upload == null || string.IsNullOrEmpty(upload.Image))
                    {
                        return BadRequest("Invalid image data.");
                    }
                    byte[] imageBytes = Convert.FromBase64String(upload.Image);
                    // Generate a unique filename
                    string fileName = upload.Name;
                    // Get the physical path to the "Uploads" directory
                    string uploadsPath = Path.Combine(_hostingEnvironment.WebRootPath, "Uploads/Images/" + upload.SessionSurveyId+"/");
                    // Create the "Uploads" directory if it doesn't exist
                    if (!Directory.Exists(uploadsPath))
                    {
                        Directory.CreateDirectory(uploadsPath);
                    }
                    // Save the image file to the "Uploads" directory
                    string filePath = Path.Combine(uploadsPath, fileName);
                    await System.IO.File.WriteAllBytesAsync(filePath, imageBytes);
                    // Return the URL of the uploaded image
                    string imageUrl = Url.Content("/Uploads/Images/" + upload.SessionSurveyId + "/"+ fileName);
                    var postImage = new PostImage
                    {
                      SessionSurveyId = upload.SessionSurveyId,
                      Location = imageUrl,
                      GlobalID = upload.GlobalID,
                    };
                    //_context.PostImages.Add(postImage);
                  
                }
                //await _context.SaveChangesAsync();
                return Ok("uploaded");
                // Decode base64 image data

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error uploading image: " + ex.Message);
            }
        }

        [HttpPost("postEndPR")]
        public async Task<IActionResult> UpdateUserFieldsWithSql([FromBody] List<PostUpdatePR> models)
        {
            
            try
            {

                foreach (PostUpdatePR _data in models)
                {
                    DateTime date;

                    DateTime.TryParseExact(_data.datePR, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                    string sql = $"UPDATE dbo.T_AWM_DataDetail SET budgetPRtanggalSelesai = '{date}'  WHERE layerName = '{_data.objType}' and globalID = '{_data.globalID}'";

                    int rowsAffected = await _context.Database.ExecuteSqlRawAsync(sql);
                }
                
                    return Ok("uploaded");
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error Update PR Budget: " + ex.Message);
            }

           
        }

        [HttpPost("postSyncBlockAncak")]
        public async Task<IActionResult> postSyncBlockAncak([FromBody] PostBlockSync models)
        {
            try
            {
                PostBlockSync _data = models;
                DateTime date;
                var db = _devHMSContext.Blocks.Where(p => p.block == _data.block & p.estCode == _data.estate).ToList();



                    //_dev.AppUpdates.Where(p => p.VersionCode == _app.Version).ToList();

                //string sql = $"UPDATE dbo.T_AWM_DataDetail SET budgetPRtanggalSelesai = '{date}'  WHERE layerName = '{_data.objType}' and globalID = '{_data.globalID}'";




                //foreach (PostBlockSync _data in models)
                //{
                //    DateTime date;

                //    DateTime.TryParseExact(_data.datePR, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                //    string sql = $"UPDATE dbo.T_AWM_DataDetail SET budgetPRtanggalSelesai = '{date}'  WHERE layerName = '{_data.objType}' and globalID = '{_data.globalID}'";

                //    int rowsAffected = await _context.Database.ExecuteSqlRawAsync(sql);
                //}

                return Ok(db);
                  

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error Update PR Budget: " + ex.Message);
            }


        }



        [HttpPost("upload")]
        public async Task<IActionResult> UploadImage([FromBody] ImageUploadModel model)
        {
            if (model == null || string.IsNullOrEmpty(model.Base64Data))
            {
                return BadRequest("Invalid image data.");
            }

            try
            {
                // Decode base64 image data
                byte[] imageBytes = Convert.FromBase64String(model.Base64Data);

                // Generate a unique filename
                string fileName = Guid.NewGuid().ToString() + ".jpg";

                // Get the physical path to the "Uploads" directory
                string uploadsPath = Path.Combine(_hostingEnvironment.WebRootPath, "Uploads");

                // Create the "Uploads" directory if it doesn't exist
                if (!Directory.Exists(uploadsPath))
                {
                    Directory.CreateDirectory(uploadsPath);
                }

                // Save the image file to the "Uploads" directory
                string filePath = Path.Combine(uploadsPath, fileName);
                await System.IO.File.WriteAllBytesAsync(filePath, imageBytes);

                // Return the URL of the uploaded image
                string imageUrl = Url.Content("~/Uploads/" + fileName);
                return Ok(imageUrl);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error uploading image: " + ex.Message);
            }
        }

        public class ImageUploadModel
        {
            public string Base64Data { get; set; }
        }


    }
}
