using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace dotnet_rpg
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<MasterAttribute,GetMasterAttributeDto>();
            CreateMap<MappingEstate,EstateMappingDto>();
            CreateMap<UserAccess, UserAccessDto>().ForMember(user => user.startDate, opt =>opt.MapFrom(src => new DateTimeOffset(src.startDate).ToUnixTimeMilliseconds()));
           
        }
    }
}