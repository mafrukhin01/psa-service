﻿using Newtonsoft.Json;
using System;

namespace PSAService.Services
{
    public class ConverterDateTime : ITypeConverter<DateTime,long>
    {
        public long Convert(DateTime source, long destination, ResolutionContext context)
        {
            return new DateTimeOffset(source).ToUnixTimeMilliseconds();
        }
    }

}
