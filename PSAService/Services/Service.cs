﻿

using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PSAService.Services.MasterDataService
{
    public class Service : IService
    {
        private readonly DataContext _context;
        private readonly SDEContext _sdeContext;
        private readonly DevHMSContext _devHMSContext;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public Service(IMapper mapper, DataContext context, SDEContext sdeContext, DevHMSContext devHMSContext, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _context = context;
            _sdeContext = sdeContext;
            _devHMSContext = devHMSContext;
            _mapper = mapper;
        }

      
        public async Task<ServiceResponse<List<GetMasterAttributeDto>>> GetMasterAttributes()
        {
            var serviceResponse = new ServiceResponse<List<GetMasterAttributeDto>>();
            var dbCharacters = await _context.MasterAttributes.ToListAsync();
            serviceResponse.Data = dbCharacters.Select(c=> _mapper.Map<GetMasterAttributeDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetMasterAttributeDto>> PostTest(MasterAttribute masterAttribute)
        {
            var response = new ServiceResponse<GetMasterAttributeDto>();
            try
            {
                _context.MasterAttributes.Add(masterAttribute);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }


        public async Task<ServiceResponse<List<GetMasterAttributeDto>>> GetAll()
        {
            var serviceResponse = new ServiceResponse<List<GetMasterAttributeDto>>();
            var dbCharacters = await _context.MasterAttributes.ToListAsync();
            serviceResponse.Data = dbCharacters.Select(c => _mapper.Map<GetMasterAttributeDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<EstateMappingDto>>> GetAllEstateMapping()
        {
            var serviceResponse = new ServiceResponse<List<EstateMappingDto>>();
            var db = await _context.EstateMapping.ToListAsync();
            if (db.Count() > 0)
            {
                serviceResponse.Data = db.Select(c => _mapper.Map<EstateMappingDto>(c)).ToList();
            }
            else
            {
                serviceResponse.Data = null;
                serviceResponse.Message = "Empty Data!";
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<EstateMappingDto>>> GetEstateMappingByEstCode(string estCode)
        {
            var serviceResponse = new ServiceResponse<List<EstateMappingDto>>();
            var db = _context.EstateMapping.Where(p => p.EstCode == estCode).ToList(); 
            if (db.Count() > 0)
            {
                serviceResponse.Data = db.Select(c => _mapper.Map<EstateMappingDto>(c)).ToList();
            }
            else
            {
                serviceResponse.Data = null;
                serviceResponse.Message = "Empty Data!";
            }
            return serviceResponse;
        }

        


        public async Task<ServiceResponse<List<LogVersioning>>> GetVersion()
        {
            var serviceResponse = new ServiceResponse<List<LogVersioning>>();
            var dbCharacters = await _context.LogVersionings.ToListAsync();
            serviceResponse.Data = dbCharacters;
            return serviceResponse;
        }

       
        public async Task<ServiceResponse<dynamic>> GetAll2()
        {
            var serviceResponse = new ServiceResponse<dynamic>();
            var dbCharacters = await _context.MasterAttributes.ToListAsync();
            var list = dbCharacters.Select(c => _mapper.Map<GetMasterAttributeDto>(c)).ToList();
            string jsonFieldValue = JsonConvert.SerializeObject(dbCharacters);
            Console.WriteLine("jsonField", (jsonFieldValue));
            Console.WriteLine("db",dbCharacters.Count);
            var obj = new Dictionary<string, object>();
            //string json = JsonConvert.SerializeObject(myJsonObject);
            obj["masterAttribute"] = dbCharacters;
            obj["masterAttribute2"] = "q";
            serviceResponse.Data = obj;
            return serviceResponse;
        }


        public async Task<ServiceResponse<List<UserAccess>>> GetUserAccess()
        {
            var serviceResponse = new ServiceResponse<List<UserAccess>>();
            var dbCharacters = await _context.UserAccesses.ToListAsync();
            serviceResponse.Data = dbCharacters;
            return serviceResponse;
        }
        public async Task<ServiceResponse<List<UserAccessDto>>> GetUserAccessDto()
        {
            var serviceResponse = new ServiceResponse<List<UserAccessDto>>();
            var dbCharacters = await _context.UserAccesses.ToListAsync();
            //var list = dbCharacters.Select(c => _mapper.Map<UserAccessDto>(c)).ToList();
            var list = dbCharacters.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
            }).ToList();
            serviceResponse.Data = list;
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<UserAccessDto>>> GetProjectsByUser(Int64 userId, string estCode)
        {
            var serviceResponse = new ServiceResponse<List<UserAccessDto>>();
            var project = _context.UserAccesses.Where(p => p.userID == userId && p.estCode == estCode).ToList();
            var list = project.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
            }).ToList();
            serviceResponse.Data = list;
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<UserAccessDto>>> GetProjectsByUser2(Int64 userId, string estCode)
        {
            var serviceResponse = new ServiceResponse<List<UserAccessDto>>();

            // Ambil data dari kedua tabel
            var project = _context.UserAccesses.Where(p => p.userID == userId && p.estCode == estCode).ToList();
            var projectAncak = _context.UserAccessAncak.Where(p => p.userID == userId && p.estCode == estCode).ToList();

            // Konversi kedua list menjadi UserAccessDto dan gabungkan
            var list = project.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
                description = "Asset Water Management"

            })
            .Concat(projectAncak.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
                description = "Survey Ancak & TPH"
            }))
            .ToList();

            // Masukkan hasil ke dalam serviceResponse
            serviceResponse.Data = list;

            return serviceResponse;
        }

        public async Task<ServiceResponse<List<UserAccessDto>>> GetProjectEStateByUser(Int64 userId, string estCode)
        {
            var serviceResponse = new ServiceResponse<List<UserAccessDto>>();
            var project = _context.UserAccesses.Where(p => p.userID == userId && p.estCode == estCode).ToList();
            var list = project.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
            }).ToList();
            var service = new ServiceResponse<List<EstateMappingDto>>();
            var db = _context.EstateMapping.Where(p => p.EstCode == estCode).ToList();
            if (db.Count() > 0)
            {
                service.Data = db.Select(c => _mapper.Map<EstateMappingDto>(c)).ToList();
            }
            else
            {
                serviceResponse.Data = null;
                serviceResponse.Message = "Empty Data!";
            }
            serviceResponse.Data = list;
            return serviceResponse;
        }


        public async Task<ServiceResponse<dynamic>> GetProjectMappingByUser(Int64 userId, string estCode)
        {
            var service = new ServiceResponse<List<EstateMappingDto>>();
            var mappingEstate = _context.EstateMapping.Where(p => p.EstCode == estCode).First();
            var serviceResponse = new ServiceResponse<dynamic>();
            var project = _context.UserAccesses.Where(p => p.userID == userId&& p.estCode == estCode).ToList();
            var list = project.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
            }).ToList();
            var obj = new Dictionary<string, object>();
            if (project.Count() > 0 && mappingEstate != null)
            {
                obj["projects"] = list;
                obj["mapping"] = mappingEstate;
                serviceResponse.Data = obj;
            }
            else
            {
                serviceResponse.Success = false;
                serviceResponse.Data = null;
                serviceResponse.Message = "Anda tidak memiliki project di Estate ini";
            }
       
            return serviceResponse;
        }

        public async Task<ServiceResponse<dynamic>> GetProjectMappingByUser2(Int64 userId, string estCode)
        {
            var serviceResponse = new ServiceResponse<dynamic>();

            // Retrieve Estate Mapping
            var mappingEstate = _context.EstateMapping.FirstOrDefault(p => p.EstCode == estCode);

            // Retrieve data from both tables
            var project = _context.UserAccesses.Where(p => p.userID == userId && p.estCode == estCode).ToList();
            var projectAncak = _context.UserAccessAncak.Where(p => p.userID == userId && p.estCode == estCode).ToList();

            // Combine data from both tables and convert to UserAccessDto
            var list = project.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
                description = "Asset Water Management"
            })
            .Concat(projectAncak.Select(c => new UserAccessDto
            {
                userID = c.userID,
                estCode = c.estCode,
                statusEdit = c.statusEdit,
                companyCode = c.companyCode,
                startDate = new DateTimeOffset(c.startDate).ToUnixTimeMilliseconds(),
                endDate = new DateTimeOffset(c.endDate).ToUnixTimeMilliseconds(),
                description = "Survey Ancak & TPH"
            }))
            .ToList();

            // Prepare the response
            var obj = new Dictionary<string, object>();
            if (list.Count > 0 && mappingEstate != null)
            {
                obj["projects"] = list;
                obj["mapping"] = mappingEstate;
                serviceResponse.Data = obj;
            }
            else
            {
                serviceResponse.Success = false;
                serviceResponse.Data = null;
                serviceResponse.Message = "Anda tidak memiliki project di Estate ini";
            }

            return serviceResponse;
        }


        public async Task<ServiceResponse<List<LogVersioning>>> PostVersion(LogVersioning newVersioning)
        {
            var serviceResponse = new ServiceResponse<List<LogVersioning>>();
            //var character = _mapper.Map<Character>(newCharacter);
            //character.User = await _context.LogVersionings.FirstOrDefaultAsync(u => u.Id == GetUserId());

            _context.LogVersionings.Add(newVersioning);
            await _context.SaveChangesAsync();

            //serviceResponse.Data = 
            //    await _context.LogVersionings
            //        .Where(c => c.IdRecord == newVersioning.IdRecord)
            //        .ToListAsync();
            return serviceResponse;
        }


        public async Task<ServiceResponse<List<MasterBlocksDto>>> GetMasterBlocksAncak(string estnr)
        {
            // Prepare service response
            var serviceResponse = new ServiceResponse<List<MasterBlocksDto>>();

            // Execute the EXEC statement
            var execQuery = "EXEC KPN_Plantation.sde.set_current_version 'Esri_Anonymous_pub_psa/Block_A'";
            _sdeContext.Database.ExecuteSqlRaw(execQuery);

            // Query for the SELECT statement
            var sqlQuery = "SELECT Block, ESTNR, Division, CompanyCode, Ha_Gross, Ha_GIS " +
                           "FROM KPN_Plantation.sde.BLK3_evw " +
                           "WHERE ESTNR = @param";

            //var sqlQuery2 = "SELECT estCode,block, multiAncak, mandor, createBy, createDate, startPeriode, endPeriode " +
            //               "FROM T_HMS_TPH_MasterBlockAncak " +
            //               "WHERE estCode = @param";


            // Execute the query with a parameter
            var resultSDE = await _sdeContext.Set<MasterBlockSDE>()
                .FromSqlRaw(sqlQuery, new SqlParameter("@param", estnr))
                .ToListAsync();

            //var resultAncak = await _devHMSContext.Set<MasterBlockAncak>()
            //    .FromSqlRaw(sqlQuery2, new SqlParameter("@param", estnr))
            //    .ToListAsync();



            // Fetch data from the second context and filter it based on estCode
            var resultAncak = await _devHMSContext.MasterBlocks
                .Where(a => a.estCode == estnr)
                .ToListAsync();




            // Join the two results based on the block field
            var joinedResult = (from sde in resultSDE
                                join ancak in resultAncak on sde.Block equals ancak.block
                                select new MasterBlocksDto
                                {
                                    estCode = ancak.estCode,
                                    block = sde.Block,
                                    Division = sde.Division,
                                    multiAncak = ancak.multiAncak??0,
                                    Ha_Gross = sde.Ha_Gross,
                                    Ha_GIS = sde.Ha_GIS,
                                    mandor = ancak.mandor,
                                    startPeriode = ancak.startPeriode.HasValue
                            ? new DateTimeOffset(ancak.startPeriode.Value).ToUnixTimeMilliseconds()
                            : (long?)null,  // Allow null values
                                    endPeriode = ancak.endPeriode.HasValue
                            ? new DateTimeOffset(ancak.endPeriode.Value).ToUnixTimeMilliseconds()
                            : (long?)null
                                }).ToList();

            // Check if any data was found
            if (joinedResult.Count > 0)
            {
                serviceResponse.Data = joinedResult;
                serviceResponse.Success = true;
                serviceResponse.Message = "Data retrieved successfully.";
            }
            else
            {
                serviceResponse.Data = null;
                serviceResponse.Success = false;
                serviceResponse.Message = "Pastikan Block masuk setting ancak";
            }

            return serviceResponse;
        }



    }
}
