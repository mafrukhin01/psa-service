﻿using Newtonsoft.Json.Linq;

namespace PSAService.Services.MasterDataService
{
    public interface IService
    {
        Task<ServiceResponse<List<GetMasterAttributeDto>>> GetMasterAttributes();
        Task<ServiceResponse<List<GetMasterAttributeDto>>> GetAll();
        Task<ServiceResponse<dynamic>> GetAll2();
        Task<ServiceResponse<List<LogVersioning>>> GetVersion();
        Task<ServiceResponse<List<LogVersioning>>> PostVersion(LogVersioning versioning);
        Task<ServiceResponse<List<UserAccess>>> GetUserAccess();
        Task<ServiceResponse<List<UserAccessDto>>> GetUserAccessDto();
        Task<ServiceResponse<dynamic>> GetProjectMappingByUser(Int64 userId, string estCode);
        Task<ServiceResponse<dynamic>> GetProjectMappingByUser2(Int64 userId, string estCode);
        Task<ServiceResponse<List<UserAccessDto>>> GetProjectsByUser(Int64 userId, string estCode);
        Task<ServiceResponse<List<UserAccessDto>>> GetProjectsByUser2(Int64 userId, string estCode);

        Task<ServiceResponse<List<EstateMappingDto>>> GetAllEstateMapping();
        Task<ServiceResponse<List<EstateMappingDto>>> GetEstateMappingByEstCode(string estCode);
        Task<ServiceResponse<List<MasterBlocksDto>>> GetMasterBlocksAncak(string estCode);


        //Task<ServiceResponse<MasterAttribute>> GetMasterAttribute(int AttributeId);
        //Task<ServiceResponse<List<MasterAttribute>>> AddMasterAttribute(MasterAttribute MasterAttribute);
    }
}
