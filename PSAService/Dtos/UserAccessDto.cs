﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using PSAService.Services;
using System;

namespace PSAService.Dtos
{
    public class UserAccessDto
    {
        public string estCode { get; set; }
        public string companyCode { get; set; }
        public Int64 userID { get; set; }
        public int statusEdit { get; set; }
        public long startDate { get; set; }
        public long endDate { get; set; }
        public string description { get; set; } = "Asset Water Management";
        //public long dateAsLong
        //{
        //    get { return startDate.Ticks; }
        //    set { startDate = new DateTime(value); }
        //}


        //public class UserAccessDtoConfiguration : IEntityTypeConfiguration<UserAccessDto>
        //{
        //    public void Configure(EntityTypeBuilder<UserAccessDto> builder)
        //    {
        //        builder.Property(m => m.startDate)
        //            .HasConversion(
        //                date => date.Ticks, // Convert DateTime to long
        //                ticks => new DateTime(ticks) // Convert long to DateTime
        //            );
        //        builder.Property(m => m.endDate)
        //            .HasConversion(
        //                date => date.Ticks, // Convert DateTime to long
        //                ticks => new DateTime(ticks) // Convert long to DateTime
        //            );
        //    }
        //}
    }
}
