﻿namespace PSAService.Dtos.MasterAttribute
{
    public class GetMasterAttributeDto
    {
        public int AttributeId { get; set; }
        public string? AliasCode { get; set; }
        public int IsMandatory { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public string? UnitOfMeasurement { get; set; }
        public string? SymbolUOM { get; set; }
    }
}
