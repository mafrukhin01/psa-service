﻿namespace PSAService.Dtos
{
    public class EstateMappingDto
    {
        public string? Plantation { get; set; }
        public string? Grouping { get; set; }
        public string? GroupCompanyName { get; set; }
        public string? CompanyCode { get; set; }
        public string? EstCode { get; set; }
        public string? EstNewCode { get; set; }
        public string? EstCodeSAP { get; set; }
        public string? NewEstName { get; set; }
    }
}
