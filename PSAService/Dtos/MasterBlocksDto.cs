﻿namespace PSAService.Dtos
{
    public class MasterBlocksDto
    {
        public string estCode { get; set; }
        public string Division { get; set; }
        public string block { get; set; }
        public int multiAncak { get; set; }
        public decimal? Ha_Gross { get; set; }
        public decimal? Ha_GIS { get; set; }
        public string mandor { get; set; }
        public long? startPeriode { get; set; } // Changed to long?
        public long? endPeriode { get; set; }

    }
}
