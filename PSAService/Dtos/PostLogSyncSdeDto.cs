﻿namespace PSAService.Dtos
{
    public class PostLogSyncSdeDto
    {
        public string? LogId
        {
            get; set;
        }
        public string? UserId
        {
            get; set;
        }
        public string? VersionSDE
        {
            get; set;
        }
        public string? Date
        {
            get; set;
        }
        public string? Action
        {
            get; set;
        } 
    }
}
